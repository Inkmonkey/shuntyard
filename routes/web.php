<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/post', 'PostController@store');
Route::get('/posts', 'PostController@index');

Route::get('/home/{any_url?}', function () {
    // We simply send the base view to the user and grab data from the server using
    // AJAX requests to our API
    return redirect('/home');
})->where('any_url', '[\/\w\.-]*');
