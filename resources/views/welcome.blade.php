@extends('layouts.auth')

@section('content')
    <div class="flex-center position-ref full-height">

        <div class="content">

            <img src="{{ url('/brand-assets/logo.png') }}" alt="FriendFace" class="logo">

            <div class="links">
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                @endif
            </div>
        </div>
    </div>
@endsection