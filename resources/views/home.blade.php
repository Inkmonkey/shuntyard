@extends('layouts.app')

@section('content')
    <div id="app">
        <div class="background-header"></div>
        <!-- Menu Module -->
        <menu-component></menu-component>
        <router-view></router-view>
        <!-- New Post Module -->
        <!-- Show Post Module -->
        <!-- Comment Module -->
        <!-- User/Friends List Module -->
        <!-- My Profile Module -->
    </div>
@endsection
