import VueRouter from 'vue-router';

const routes = [
    {
        path: '/',
        component: require('../views/wall-view.vue') 
    },
    {
        path: '/my-friends',
        component: require('../views/friends-view.vue') 
    },
    {
        path: '/find-friends',
        component: require('../views/find-friends-view.vue') 
    },
]

export default new VueRouter({
    routes,
    base: '/home/',
	linkActiveClass: 'is-Active',
	hashbang: false,
	mode: 'history',
})

