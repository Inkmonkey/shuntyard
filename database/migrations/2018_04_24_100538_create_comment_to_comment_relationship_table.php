<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentToCommentRelationshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_comment', function (Blueprint $table) {
            $table->integer('parent_comment_id')->unsigned();
            $table->foreign('parent_comment_id')->references('id')->on('comments');

            $table->integer('child_comment_id')->unsigned();
            $table->foreign('child_comment_id')->references('id')->on('comments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comment_comment', function (Blueprint $table) {
            $table->dropForeign(['parent_comment_id']);
            $table->dropForeign(['child_comment_id']);
        });

        Schema::dropIfExists('comment_comment');
    }
}
