# Shuntyard Assessment Project

Assessment for Shuntyard Technologies.

First install using `composer install`, then `yarn` or `npm install`. Next, run the project on your local machine with `php artisan serve` (needs php installed).

For more information check out the Laravel Documentation: https://laravel.com/docs/5.6